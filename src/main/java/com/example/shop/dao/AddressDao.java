package com.example.shop.dao;

import com.example.shop.entity.Address;
import com.example.shop.exception.DaoException;

import javax.ejb.Local;
import java.util.List;

@Local
public interface AddressDao {
    void add(Address address) throws DaoException;

    void update(Address userEntity) throws DaoException;

    void delete(Address address) throws DaoException;

    void delete(int id) throws DaoException;

    Address get(int id) throws DaoException;

    List<Address> getAll() throws DaoException;
}
