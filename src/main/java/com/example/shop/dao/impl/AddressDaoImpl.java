package com.example.shop.dao.impl;

import com.example.shop.dao.AddressDao;
import com.example.shop.entity.Address;
import com.example.shop.exception.DaoException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Stateless
public class AddressDaoImpl implements AddressDao {

    @PersistenceContext(unitName = "myUnit")
    EntityManager entityManager;

    @Override
    public void add(Address address) throws DaoException {
        try {
            if (address.getId() == 0) {
                entityManager.persist(address);
            } else {
                entityManager.merge(address);
            }
        } catch (IllegalStateException | PersistenceException e) {
            throw new DaoException(e);
        }
    }
   
    @Override
    public void update(Address address) throws DaoException {
        try {
            entityManager.merge(address);
        } catch (IllegalStateException | PersistenceException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void delete(Address address) throws DaoException {
        try {
            entityManager.remove(entityManager.contains(address) ? address : entityManager.merge(address));
        } catch (IllegalStateException | PersistenceException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void delete(int id) throws DaoException {
        Address address = get(id);
        delete(address);
    }

    @Override
    public Address get(int id) throws DaoException {
        try {
            Address address = entityManager.find(Address.class, id);
            return address;
        } catch (IllegalStateException | PersistenceException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<Address> getAll() throws DaoException {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Address> criteriaQuery = criteriaBuilder.createQuery(Address.class);
        Root<Address> root = criteriaQuery.from(Address.class);
        CriteriaQuery<Address> all = criteriaQuery.select(root);
        TypedQuery<Address> allQuery = entityManager.createQuery(all);

        List<Address> list = allQuery.getResultList();
        return list;

    }
}
