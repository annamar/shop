package com.example.shop.exception;

import javax.ws.rs.WebApplicationException;

public class ValidationException extends WebApplicationException {
    public ValidationException() {
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException(Throwable cause) {
        super(cause);
    }
}
