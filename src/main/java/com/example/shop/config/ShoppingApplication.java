package com.example.shop.config;

import com.example.shop.resource.CustomerResource;
import com.example.shop.resource.impl.MyService;
import com.example.shop.resource.impl.CustomerResourceImpl;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/services")
public class ShoppingApplication extends Application {
    private Set<Object> singletons = new HashSet<Object>();
    private Set<Class<?>> empty = new HashSet<Class<?>>();

    public ShoppingApplication() {
        singletons.add(new CustomerResourceImpl());
        singletons.add(new MyService());
        empty.add(MyService.class);
        empty.add(CustomerResourceImpl.class);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return empty;
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
