package com.example.shop.config;

import com.example.shop.exception.DaoException;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DaoMapper implements ExceptionMapper<DaoException> {
    @Override
    public Response toResponse(DaoException exception) {
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
