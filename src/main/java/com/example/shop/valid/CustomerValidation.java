package com.example.shop.valid;

import com.example.shop.entity.Customer;
import com.example.shop.exception.ValidationException;

import javax.ejb.Stateless;

@Stateless
public class CustomerValidation {
    public void validate(Customer customer) throws ValidationException {
        String message = "";
        if (customer.getId() < 0) {
            message += "Id < 0;";
        }
        if (customer.getFirstName() == null || customer.getFirstName().length() < 3) {
            message += "First name is invalid";
        }
        if (customer.getLastName() == null || customer.getLastName().length() < 3) {
            message += "First name is invalid";
        }
        if (!message.isEmpty()) {
            throw new ValidationException(message);
        }
    }
}
