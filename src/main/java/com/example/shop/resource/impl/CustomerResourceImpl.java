package com.example.shop.resource.impl;

import com.example.shop.entity.Customer;
import com.example.shop.exception.DaoException;
import com.example.shop.exception.ValidationException;
import com.example.shop.resource.CustomerResource;
import com.example.shop.service.CustomerService;
import com.example.shop.service.impl.CustomerServiceImpl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.List;

@Stateless
public class CustomerResourceImpl implements CustomerResource {

    @EJB
    CustomerService service;

    @Override
    public Response createCustomer(Customer customer) throws ValidationException, DaoException {

        if (service.add(customer)) {
            return Response.status(Response.Status.CREATED).entity(customer).build();
        } else {
            return Response.notModified().build();
        }
    }

    @Override
    public Response updateCustomer(Customer customer) throws ValidationException, DaoException {
        if (service.update(customer)) {
            return Response.status(Response.Status.OK).entity(customer).build();
        } else {
            return Response.notModified().build();
        }
    }

    @Override
    public Response deleteCustomer(int id) throws ValidationException, DaoException {
        if (service.delete(id)) {
            return Response.status(Response.Status.OK).entity(service.get(id)).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

    }

    @Override
    public Response getCustomer(int id) throws ValidationException, DaoException {
        Customer customer = service.get(id);
        if (customer != null) {
            return Response.status(Response.Status.OK).entity(customer).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @Override
    public Response getAll() throws DaoException {
        List<Customer> customers = service.getAll();
        if (!customers.isEmpty()) {
            return Response.ok(customers).build();
        } else {
            return Response.noContent().build();
        }
    }
}
