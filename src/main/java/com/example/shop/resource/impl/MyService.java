package com.example.shop.resource.impl;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;

@Path("/myservice")
public class MyService {
    @GET
    @Path("/hello")
    @Produces("text/plain")
    public StreamingOutput get() {
        return new StreamingOutput() {
            public void write(OutputStream output) throws IOException, WebApplicationException {
                output.write("Heelo World".getBytes());
            }
        };
    }

}
