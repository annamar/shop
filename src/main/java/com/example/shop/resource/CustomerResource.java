package com.example.shop.resource;

import com.example.shop.entity.Customer;
import com.example.shop.exception.DaoException;
import com.example.shop.exception.ValidationException;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/customers")
public interface CustomerResource {

    @POST
    @Path("/add")
    @Consumes({"application/json", "application/xml"})
    public Response createCustomer(Customer customer) throws ValidationException, DaoException;

    @PUT
    @Path("/{id}")
    @Consumes({"application/json", "application/xml"})
    public Response updateCustomer(Customer customer) throws ValidationException, DaoException;

    @DELETE
    @Path("/{id}")
    @Produces({"application/json", "application/xml"})
    public Response deleteCustomer(@PathParam("id") int id) throws ValidationException, DaoException;

    @GET
    @Path("/{id}")
    @Produces({"application/json", "application/xml"})
    public Response getCustomer(@PathParam("id") int id) throws ValidationException, DaoException;

    @GET
    @Produces({"application/json", "application/xml"})
    public Response getAll() throws DaoException;
}
