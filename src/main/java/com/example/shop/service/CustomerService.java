package com.example.shop.service;

import com.example.shop.entity.Customer;
import com.example.shop.exception.DaoException;
import com.example.shop.exception.ValidationException;

import javax.ejb.Local;
import java.util.List;
@Local
public interface CustomerService {
    boolean add(Customer customer) throws ValidationException, DaoException;
    boolean delete(int id) throws ValidationException, DaoException;
    boolean delete(Customer customer) throws ValidationException, DaoException;
    boolean update(Customer customer) throws ValidationException, DaoException;
    Customer get(int id) throws ValidationException, DaoException;
    List<Customer> getAll() throws DaoException;
}
