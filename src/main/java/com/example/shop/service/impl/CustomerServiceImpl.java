package com.example.shop.service.impl;

import com.example.shop.dao.CustomerDao;
import com.example.shop.dao.impl.CustomerDaoImpl;
import com.example.shop.entity.Customer;
import com.example.shop.exception.DaoException;
import com.example.shop.exception.ValidationException;
import com.example.shop.service.CustomerService;
import com.example.shop.valid.CustomerValidation;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class CustomerServiceImpl implements CustomerService {

    @EJB
    CustomerDao dao;

    @EJB
    CustomerValidation validation;

    @Override
    public boolean add(Customer customer) throws ValidationException, DaoException {
        validation.validate(customer);
        dao.add(customer);
        return true;
    }

    @Override
    public boolean delete(int id) throws ValidationException, DaoException {
        if (id < 0) throw new ValidationException("Id < 0");
        dao.delete(id);
        return true;
    }

    @Override
    public boolean delete(Customer customer) throws ValidationException, DaoException {
        validation.validate(customer);
        dao.delete(customer);
        return true;
    }

    @Override
    public boolean update(Customer customer) throws ValidationException, DaoException {
        validation.validate(customer);
        dao.update(customer);
        return true;
    }

    @Override
    public Customer get(int id) throws ValidationException, DaoException {
        if (id < 0) throw new ValidationException("Id < 0");
        return dao.get(id);
    }

    @Override
    public List<Customer> getAll() throws DaoException {
        return dao.getAll();
    }
}
