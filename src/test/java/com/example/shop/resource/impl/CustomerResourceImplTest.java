package com.example.shop.resource.impl;

import com.example.shop.entity.Customer;
import com.example.shop.exception.DaoException;
import com.example.shop.exception.ValidationException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class CustomerResourceImplTest {

    private static Client client;

    @BeforeClass
    public static void setup() {
        client = ClientBuilder.newClient();
    }

    @Test
    public void testCreateCustomerSuccess() throws ValidationException, DaoException {

        String xml = "<customer><address><id>1</id></address><firstName>DDD</firstName><lastName>DDDDD</lastName></customer>";
        Response response = client
                .target("http://localhost:8082/shop-1.0-SNAPSHOT/services/customers/add")
                .request()
                .post(Entity.xml(xml));
        Assert.assertEquals(response.getStatus(), 201);
    }

    @Test
    public void testCreateCustomerFail() throws ValidationException, DaoException {
        String xml = "<customer><address><id>-1</id></address><firstName>DDD</firstName><lastName>DDDDD</lastName></customer>";
        Response response = client
                .target("http://localhost:8082/shop-1.0-SNAPSHOT/services/customers/add")
                .request()
                .post(Entity.xml(xml));
        Assert.assertEquals(response.getStatus(), 500);
    }

    @Test
    public void getCustomer(){
        Response response = client
                .target("http://localhost:8082/shop-1.0-SNAPSHOT/services/customers/4")
                .request()
                .get();
        String customer = response.readEntity(String.class);
        System.out.println(customer);
    }

    @Test
    public void testUpdateCustomer() {
        String xml = "<customer><address><id>1</id></address><firstName>DDD</firstName><lastName>DDDDD</lastName></customer>";
        Response response = client
                .target("http://localhost:8082/shop-1.0-SNAPSHOT/services/customers/add")
                .request()
                .post(Entity.xml(xml));
        if (response.getStatus() != 201) throw new RuntimeException("Failed	to	create");
        String customer =  response.readEntity(String.class);
        response.close();
        String updateCustomer = "<customer><first-name>William</first-name><last-name>Burke</last-name><address><id>12</id></address></customer>";
        response = client
                .target("http://localhost:8082/shop-1.0-SNAPSHOT/services/customers/")
//                .path(String.valueOf(id))
                .request()
                .put(Entity.xml(updateCustomer));
        if (response.getStatus() != 204) throw new RuntimeException("Failed	to	update");
    }
}