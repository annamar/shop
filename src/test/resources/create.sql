CREATE TABLE IF NOT EXISTS  `address`
(
    `id`     int auto_increment
        primary key,
    `city`    varchar(255) NULL,
    `country` varchar(255) NULL,
    `state`   varchar(255) NULL,
    `street`  varchar(255) NULL,
    `zip`     varchar(255) NULL
);

CREATE TABLE IF NOT EXISTS `CUSTOMER` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) NULL,
  `lastName` varchar(255) NULL,
  `address_id` int(11) NULL,
  constraint FKfok4ytcqy7lovuiilldbebpd9
        foreign key (address_id) references address (id),
  PRIMARY KEY (`id`)
);